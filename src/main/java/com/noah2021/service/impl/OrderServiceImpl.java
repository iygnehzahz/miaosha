package com.noah2021.service.impl;

import com.noah2021.dao.OrderDOMapper;
import com.noah2021.dao.SequenceDOMapper;
import com.noah2021.dao.StockLogDOMapper;
import com.noah2021.error.BusinessException;
import com.noah2021.error.EmBusinessError;
import com.noah2021.mq.MqProducer;
import com.noah2021.pojo.OrderDO;
import com.noah2021.pojo.SequenceDO;
import com.noah2021.pojo.StockLogDO;
import com.noah2021.service.ItemService;
import com.noah2021.service.OrderService;
import com.noah2021.service.UserService;
import com.noah2021.service.model.ItemModel;
import com.noah2021.service.model.OrderModel;
import com.noah2021.service.model.UserModel;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/2/9
 * @return
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private SequenceDOMapper sequenceDOMapper;

    @Autowired
    private ItemService itemService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrderDOMapper orderDOMapper;

    @Autowired
    private StockLogDOMapper stockLogDOMapper;

    @Override
    @Transactional
    public OrderModel createOrder(Integer userId, Integer itemId, Integer promoId, Integer amount, String stockLogId) throws BusinessException {
        //入参校验
        //ItemModel itemModel = itemService.getItemById(itemId);
        ItemModel itemModel = itemService.getItemByIdInCache(itemId);
        if (itemModel == null) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "商品信息不存在");
        }
//
//        UserModel userModel = userService.getUserByIdInCache(userId);
//        if (userModel == null) {
//            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "用户信息不存在");
//        }
//        if (amount <= 0 || amount > 99) {
//            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "数量信息不正确");
//        }
//        if (promoId != null) {
//            if (promoId.intValue() != itemModel.getPromoModel().getId())
//                throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "活动信息不正确");
//            else if (itemModel.getPromoModel().getStatus() != 2)
//                throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "数量信息未开始");
//        }


        //下单方式：1.落单减库存    2.支付减库存：会造成某人已下完单，但是当付款成功的时候却没货
        boolean flag = itemService.decreaseStock(itemId, amount);
        if (!flag)
            throw new BusinessException(EmBusinessError.STOCK_NOT_ENOUGH);
        //订单入库
        OrderModel orderModel = new OrderModel();
        orderModel.setUserId(userId);
        orderModel.setItemId(itemId);
        orderModel.setAmount(amount);
        if (promoId != null)
            orderModel.setItemPrice(itemModel.getPromoModel().getPromoItemPrice());
        else
            orderModel.setItemPrice(itemModel.getPrice());

        orderModel.setPromoId(promoId);
        //这里不再是商品价格itemModel.getPrice()而是之前订单价格orderModel.getItemPrice()
        BigDecimal orderPrice = orderModel.getItemPrice().multiply(new BigDecimal(amount));
        orderModel.setOrderPrice(orderPrice);
        orderModel.setId(generateOrderNo());
        //返回前端
        OrderDO orderDO = convertFromOrderModel(orderModel);
        //插入到orderinfo表
        orderDOMapper.insertSelective(orderDO);
        //增加销量
        itemService.increaseSales(itemId, amount);

        //设置库存流水状态为成功
        StockLogDO stockLogDO = stockLogDOMapper.selectByPrimaryKey(stockLogId);
        if(stockLogDO == null)
            throw new BusinessException(EmBusinessError.UNKNOWN_ERROR);
        stockLogDO.setStatus(2);
        stockLogDOMapper.updateByPrimaryKeySelective(stockLogDO);
//        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
//            @Override
//            public void afterCommit() {
//                //异步更新库存
//                boolean mqResult = itemService.asyncDecreaseStock(itemId, amount);
////                if (!mqResult) {
////                    itemService.increaseStock(itemId, amount);
////                    throw new BusinessException(EmBusinessError.MQ_SEND_FAIL);
////                }
//            }
//        });

        return orderModel;
    }


    //生成商品订单流水号
    /*
    public enum Propagation {
    REQUIRED(0),   //表示开启一个事务并在事务之中，当已经在一个事务中就可以不用开启
    SUPPORTS(1),
    MANDATORY(2),
    REQUIRES_NEW(3),  //无论有没有开启事务都必须重新开启一个新的事务
    NOT_SUPPORTED(4),
    NEVER(5),
    NESTED(6);
    * */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String generateOrderNo() {
        StringBuilder stringBuilder = new StringBuilder();
        LocalDateTime now = LocalDateTime.now();
        String nowDate = now.format(DateTimeFormatter.ISO_DATE).replace("-", "");
        stringBuilder.append(nowDate);

        int sequence = 0;
        SequenceDO sequenceDO = sequenceDOMapper.getSequenceByName("order_info");
        sequence = sequenceDO.getCurrentValue();
        sequenceDO.setCurrentValue(sequenceDO.getCurrentValue() + sequenceDO.getStep());
        //修改sequence_info的current_value
        sequenceDOMapper.updateByPrimaryKeySelective(sequenceDO);
        String sequenceStr = String.valueOf(sequence);
        for (int i = 0; i < 6 - sequenceStr.length(); i++)
            stringBuilder.append(0);
        stringBuilder.append(sequenceStr);
        stringBuilder.append("00");
        return stringBuilder.toString();
    }

    private OrderDO convertFromOrderModel(OrderModel orderModel) {
        if (orderModel == null) {
            return null;
        }
        OrderDO orderDO = new OrderDO();
        BeanUtils.copyProperties(orderModel, orderDO);
        orderDO.setItemPrice(orderModel.getItemPrice().doubleValue());
        orderDO.setOrderPrice(orderModel.getOrderPrice().doubleValue());
        return orderDO;
    }

}